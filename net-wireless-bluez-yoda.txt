>>> Completed (7 of 7) www-servers/apache-2.4.58-r2::gentoo

 * Messages for package net-wireless/bluez-5.72:

 *   CONFIG_BT:	 is not set when it should be.
 *   CONFIG_BT_RFCOMM:	 is not set when it should be.
 *   CONFIG_BT_RFCOMM_TTY:	 is not set when it should be.
 *   CONFIG_BT_BNEP:	 is not set when it should be.
 *   CONFIG_BT_BNEP_MC_FILTER:	 is not set when it should be.
 *   CONFIG_BT_BNEP_PROTO_FILTER:	 is not set when it should be.
 *   CONFIG_BT_HIDP:	 is not set when it should be.
 *   CONFIG_CRYPTO_USER_API_SKCIPHER:	 is not set when it should be.
 *   CONFIG_UHID:	 is not set when it should be.
 *   CONFIG_CRYPTO_USER:	 is not set when it should be.
 *   CONFIG_CRYPTO_USER_API_AEAD:	 is not set when it should be.
 * Please check to make sure these options are set correctly.
 * Failure to do so may cause unexpected problems.
 * To use dial up networking you must install net-dialup/ppp

 * Messages for package www-servers/apache-2.4.58-r2:

 * 
 * Selected default MPM: prefork
 * 
 * Please note that you need SysV IPC support in your kernel.
 * Make sure CONFIG_SYSVIPC=y is set.
 * 
 
 
  Symbol: BT [=n]                                                                                                                                                                       │  
  │ Type  : tristate                                                                                                                                                                      │  
  │ Defined at net/bluetooth/Kconfig:6                                                                                                                                                    │  
  │   Prompt: Bluetooth subsystem support                                                                                                                                                 │  
  │   Depends on: NET [=y] && !S390 && (RFKILL [=y] || !RFKILL [=y])                                                                                                                      │  
  │   Location:                                                                                                                                                                           │  
  │     -> Networking support (NET [=y])                                                                                                                                                  │  
  │ (1)   -> Bluetooth subsystem support (BT [=n])                                                                                                                                        │  
  │ Selects: CRC16 [=y] && CRYPTO [=y] && CRYPTO_SKCIPHER [=y] && CRYPTO_LIB_AES [=y] && CRYPTO_CMAC [=y] && CRYPTO_ECB [=n] && CRYPTO_SHA256 [=y] && CRYPTO_ECDH [=n]                    │  
  │ Implies: CRYPTO_AES [=y]                                                                                                                                                              │  
  │                    
  
    │ Symbol: BT_RFCOMM [=n]                                                                                                                                                                │  
  │ Type  : tristate                                                                                                                                                                      │  
  │ Defined at net/bluetooth/rfcomm/Kconfig:2                                                                                                                                             │  
  │   Prompt: RFCOMM protocol support                                                                                                                                                     │  
  │   Depends on: NET [=y] && BT_BREDR [=y]                                                                                                                                               │  
  │   Location:                                                                                                                                                                           │  
  │     -> Networking support (NET [=y])                                                                                                                                                  │  
  │       -> Bluetooth subsystem support (BT [=y])                                                                                                                                        │  
  │         -> Bluetooth Classic (BR/EDR) features (BT_BREDR [=y])                                                                                                                        │  
  │ (1)       -> RFCOMM protocol support (BT_RFCOMM [=n])                                                                                                                                 │  
  │                                                                                                                                                                                       │  
  │                                                                                                                                                                                       │  
  │ Symbol: BT_RFCOMM_TTY [=n]                                                                                                                                                            │  
  │ Type  : bool                                                                                                                                                                          │  
  │ Defined at net/bluetooth/rfcomm/Kconfig:13                                                                                                                                            │  
  │   Prompt: RFCOMM TTY support                                                                                                                                                          │  
  │   Depends on: NET [=y] && BT_RFCOMM [=n] && TTY [=y]                                                                                                                                  │  
  │   Location:                                                                                                                                                                           │  
  │     -> Networking support (NET [=y])                                                                                                                                                  │  
  │       -> Bluetooth subsystem support (BT [=y])                                                                                                                                        │  
  │         -> Bluetooth Classic (BR/EDR) features (BT_BREDR [=y])                                                                                                                        │  
  │ (2)       -> RFCOMM protocol support (BT_RFCOMM [=n])                                                                                                                                 │  
  │             -> RFCOMM TTY support (BT_RFCOMM_TTY [=n])       
  
  
  
    ┌─────────────────────────────────────────────────────────────────────────────────── Search Results ────────────────────────────────────────────────────────────────────────────────────┐
  │ Symbol: CRYPTO_USER_API_SKCIPHER [=n]                                                                                                                                                 │  
  │ Type  : tristate                                                                                                                                                                      │  
  │ Defined at crypto/Kconfig:1341                                                                                                                                                        │  
  │   Prompt: Symmetric key cipher algorithms                                                                                                                                             │  
  │   Depends on: CRYPTO [=y] && NET [=y]                                                                                                                                                 │  
  │   Location:                                                                                                                                                                           │  
  │     -> Cryptographic API (CRYPTO [=y])                                                                                                                                                │  
  │       -> Userspace interface                                                                                                                                                          │  
  │ (1)     -> Symmetric key cipher algorithms (CRYPTO_USER_API_SKCIPHER [=n])                                                                                                            │  
  │ Selects: CRYPTO_SKCIPHER [=y] && CRYPTO_USER_API [=y]   
  
  
  
    │ Symbol: UHID [=n]                                                                                                                                                                     │  
  │ Type  : tristate                                                                                                                                                                      │  
  │ Defined at drivers/hid/Kconfig:63                                                                                                                                                     │  
  │   Prompt: User-space I/O driver support for HID subsystem                                                                                                                             │  
  │   Depends on: HID_SUPPORT [=y] && HID [=y]                                                                                                                                            │  
  │   Location:                                                                                                                                                                           │  
  │     -> Device Drivers                                                                                                                                                                 │  
  │       -> HID bus support (HID_SUPPORT [=y])                                                                                                                                           │  
  │         -> HID bus core support (HID [=y])                                                                                                                                            │  
  │ (1)       -> User-space I/O driver support for HID subsystem (UHID [=n])                                                                                                              │  
  │                                                                                                                                                                                       │  
  │                                                                                                                                                                                       │  
  │ Symbol: SAMPLE_UHID [=n]                                                                                                                                                              │  
  │ Type  : bool                                                                                                                                                                          │  
  │ Defined at samples/Kconfig:187                                                                                                                                                        │  
  │   Prompt: UHID sample                                                                                                                                                                 │  
  │   Depends on: SAMPLES [=n] && CC_CAN_LINK [=y] && HEADERS_INSTALL [=n]                                                                                                                │  
  │   Location:                                                                                                                                                                           │  
  │     -> Kernel hacking                                                                                                                                                                 │  
  │ (2)   -> Sample kernel code (SAMPLES [=n])                                                                                                                                            │  
  │         -> UHID sample (SAMPLE_UHID [=n])                                                                                                                                             │  
  │                                             
