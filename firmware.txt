 * Messages for package sys-kernel/linux-firmware-20230310-r1:

 * Your configuration for sys-kernel/linux-firmware-20230310-r1 has been saved in 
 * "/etc/portage/savedconfig/sys-kernel/linux-firmware-20230310-r1" for your editing pleasure.
 * You can edit these files by hand and remerge this package with
 * USE=savedconfig to customise the configuration.
 * You can rename this file/directory to one of the following for
 * its configuration to apply to multiple versions:
 * ${PORTAGE_CONFIGROOT}/etc/portage/savedconfig/
 * [${CTARGET}|${CHOST}|""]/${CATEGORY}/[${PF}|${P}|${PN}]
 * If you are only interested in particular firmware files, edit the saved
 * configfile and remove those that you do not want.




 * Messages for package sys-kernel/linux-firmware-20221214:

 * Your configuration for sys-kernel/linux-firmware-20221214 has been saved in 
 * "/etc/portage/savedconfig/sys-kernel/linux-firmware-20221214" for your editing pleasure.
 * You can edit these files by hand and remerge this package with
 * USE=savedconfig to customise the configuration.
 * You can rename this file/directory to one of the following for
 * its configuration to apply to multiple versions:
 * ${PORTAGE_CONFIGROOT}/etc/portage/savedconfig/
 * [${CTARGET}|${CHOST}|""]/${CATEGORY}/[${PF}|${P}|${PN}]
 * If you are only interested in particular firmware files, edit the saved
 * configfile and remove those that you do not want.

