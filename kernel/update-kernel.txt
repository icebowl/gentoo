https://wiki.gentoo.org/wiki/Kernel/Upgrade

Installed "emerge -av sys-kernel/installkernel" 
to use "make install"


emerge --pretend --verbose sys-kernel/installkernel ; grep sys-kernel/installkernel /var/log/emerge.log | tail -n20?

[ebuild N ] sys-kernel/installkernel-28::gentoo USE="-dracut -grub -refind -systemd -systemd-boot -uki -ukify" 19 KiB

Total: 1 package (1 new), Size of downloads: 19 KiB
1706891783: >>> emerge (1 of 9) sys-kernel/installkernel-24 to /
1706891783: === (1 of 9) Cleaning (sys-kernel/installkernel-24::/var/db/repos/gentoo/sys-kernel/installkernel/installkernel-24.ebuild)
1706891783: === (1 of 9) Compiling/Merging (sys-kernel/installkernel-24::/var/db/repos/gentoo/sys-kernel/installkernel/installkernel-24.ebuild)
1706891793: === (1 of 9) Merging (sys-kernel/installkernel-24::/var/db/repos/gentoo/sys-kernel/installkernel/installkernel-24.ebuild)
1706891797: >>> AUTOCLEAN: sys-kernel/installkernel:0
1706891797: === Unmerging... (sys-kernel/installkernel-19)
1706891800: >>> unmerge success: sys-kernel/installkernel-19
1706891806: === (1 of 9) Post-Build Cleaning (sys-kernel/installkernel-24::/var/db/repos/gentoo/sys-kernel/installkernel/installkernel-24.ebuild)
1706891806: ::: completed emerge (1 of 9) sys-kernel/installkernel-24 to /
1709996750: >>> emerge (1 of 3) sys-kernel/installkernel-25 to /
1709996751: === (1 of 3) Cleaning (sys-kernel/installkernel-25::/var/db/repos/gentoo/sys-kernel/installkernel/installkernel-25.ebuild)
1709996751: === (1 of 3) Compiling/Merging (sys-kernel/installkernel-25::/var/db/repos/gentoo/sys-kernel/installkernel/installkernel-25.ebuild)
1709996760: === (1 of 3) Merging (sys-kernel/installkernel-25::/var/db/repos/gentoo/sys-kernel/installkernel/installkernel-25.ebuild)
1709996764: >>> AUTOCLEAN: sys-kernel/installkernel:0
1709996764: === Unmerging... (sys-kernel/installkernel-24)
1709996767: >>> unmerge success: sys-kernel/installkernel-24
1709996770: === (1 of 3) Post-Build Cleaning (sys-kernel/installkernel-25::/var/db/repos/gentoo/sys-kernel/installkernel/installkernel-25.ebuild)
1709996770: ::: completed emerge (1 of 3) sys-kernel/installkernel-25 to /
1710347039: === Unmerging... (sys-kernel/installkernel-25)
1710347042: >>> unmerge success: sys-kernel/installkernel-25
yoda ~ #
