CONFIG_BRIDGE_EBT_MARK_T
 *   CONFIG_BRIDGE_NF_EBTABLES:	 is not set when it should be.
 *   CONFIG_NETFILTER_ADVANCED:	 is not set when it should be.
 *   CONFIG_NETFILTER_XT_CONNMARK:	 is not set when it should be.
 *   CONFIG_NETFILTER_XT_TARGET_CHECKSUM:	 is not set when it should be.
 *   CONFIG_IP6_NF_NAT:	 is not set when it should be.
 *   CONFIG_BRIDGE_EBT_T_NAT:	 is not set when it should be.
 *   CONFIG_NET_ACT_POLICE:	 is not set when it should be.
 *   CONFIG_NET_CLS_FW:	 is not set when it should be.
 *   CONFIG_NET_CLS_U32:	 is not set when it should be.
 *   CONFIG_NET_SCH_HTB:	 is not set when it should be.
 *   CONFIG_NET_SCH_INGRESS:	 is not set when it should be.
 *   CONFIG_NET_SCH_SFQ:	 is not set when it should be.

app-emulation/libvirt-9.8.0-r2:

:	 is not set when it should be.
done *   CONFIG_BRIDGE_NF_EBTABLES:	 is not set when it should be.
 Symbol: BRIDGE_NF_EBTABLES [=n]                                             │  
  │ Type  : tristate                                                            │  
  │ Defined at net/bridge/netfilter/Kconfig:42                                  │  
  │   Prompt: Ethernet Bridge tables (ebtables) support                         │  
  │   Depends on: NET [=y] && BRIDGE [=y] && NETFILTER [=y] && NETFILTER_XTABLE │  
  │   Location:                                                                 │  
  │     -> Networking support (NET [=y])                                        │  
  │       -> Networking options                                                 │  
  │         -> Network packet filtering framework (Netfilter) (NETFILTER [=y])  │  
  │ (1)       -> Ethernet Bridge tables (ebtables) support (BRIDGE_NF_EBTABLES  │  
  │ Selects: NETFILTER_FAMILY_BRIDGE [=n]   
 
 
done *   CONFIG_NETFILTER_ADVANCED:	 is not set when it should be.
  Symbol: NETFILTER_ADVANCED [=n]                                             │  
  │ Type  : bool                                                                │  
  │ Defined at net/Kconfig:202                                                  │  
  │   Prompt: Advanced netfilter configuration                                  │  
  │   Depends on: NET [=y] && NETFILTER [=y]                                    │  
  │   Location:                                                                 │  
  │     -> Networking support (NET [=y])                                        │  
  │       -> Networking options                                                 │  
  │         -> Network packet filtering framework (Netfilter) (NETFILTER [=y])  │  
  │ (1)       -> Advanced netfilter configuration (NETFILTER_ADVANCED [=n])     │  
  │                                                                             │  
  │ 
  
 done *   CONFIG_NETFILTER_XT_CONNMARK:	 is not set when it should be.
  Symbol: NETFILTER_XT_CONNMARK [=n]                                                                                                                                                    │  
  │ Type  : tristate                                                                                                                                                                      │  
  │ Defined at net/netfilter/Kconfig:781                                                                                                                                                  │  
  │   Prompt: ctmark target and match support                                                                                                                                             │  
  │   Depends on: NET [=y] && INET [=y] && NETFILTER [=y] && NETFILTER_XTABLES [=y] && NF_CONNTRACK [=y] && NETFILTER_ADVANCED [=y]                                                       │  
  │   Location:                                                                                                                                                                           │  
  │     -> Networking support (NET [=y])                                                                                                                                                  │  
  │       -> Networking options                                                                                                                                                           │  
  │         -> Network packet filtering framework (Netfilter) (NETFILTER [=y])                                                                                                            │  
  │           -> Core Netfilter Configuration                                                                                                                                             │  
  │             -> Netfilter Xtables support (required for ip_tables) (NETFILTER_XTABLES [=y])                                                                                            │  
  │ (1)           -> ctmark target and match support (NETFILTER_XT_CONNMARK [=n])                                                                                                         │  
  │ Selects: NF_CONNTRACK_MARK [=n]                                                                                                                                                       │  
  │ Selected by [n]:                                                                                                                                                                      │  
  │   - NETFILTER_XT_TARGET_CONNMARK [=n] && NET [=y] && INET [=y] && NETFILTER [=y] && NETFILTER_XTABLES [=y] && NF_CONNTRACK [=y] && NETFILTER_ADVANCED [=y]                            │  
  │   - NETFILTER_XT_MATCH_CONNMARK [=n] && NET [=y] && INET [=y] && NETFILTER [=y] && NETFILTER_XTABLES [=y] && NF_CONNTRACK [=y] && NETFILTER_ADVANCED [=y]                             │  
  │                                                                                                                                                               
 
 
 *   CONFIG_NETFILTER_XT_TARGET_CHECKSUM:	 is not set when it should be.
  │ Symbol: NETFILTER_XT_TARGET_CHECKSUM [=n]                                           │  
  │ Type  : tristate                                                                    │  
  │ Defined at net/netfilter/Kconfig:819                                                │  
  │   Prompt: CHECKSUM target support                                                   │  
  │   Depends on: NET [=y] && INET [=y] && NETFILTER [=y] && NETFILTER_XTABLES [=y] &&  │  
  │   Location:                                                                         │  
  │     -> Networking support (NET [=y])                                                │  
  │       -> Networking options                                                         │  
  │         -> Network packet filtering framework (Netfilter) (NETFILTER [=y])          │  
  │           -> Core Netfilter Configuration                                           │  
  │             -> Netfilter Xtables support (required for ip_tables) (NETFILTER_XTABLE │  
  │ (1)           -> CHECKSUM target support (NETFILTER_XT_TARGET_CHECKSUM [=n])
 
 
 *  /:	 is not set when it should be.
 
 *   CONFIG_BRIDGE_EBT_T_NAT:	 is not set when it should be.
	 Symbol: BRIDGE_EBT_T_NAT [=n]                                                       │  
  │ Type  : tristate                                                                    │  
  │ Defined at net/bridge/netfilter/Kconfig:75                                          │  
  │   Prompt: ebt: nat table support                                                    │  
  │   Depends on: NET [=y] && NETFILTER [=y] && BRIDGE_NF_EBTABLES [=y]                 │  
  │   Location:                                                                         │  
  │     -> Networking support (NET [=y])                                                │  
  │       -> Networking options                                                         │  
  │         -> Network packet filtering framework (Netfilter) (NETFILTER [=y])          │  
  │           -> Ethernet Bridge tables (ebtables) support (BRIDGE_NF_EBTABLES [=y])    │  
  │ (1)         -> ebt: nat table support (BRIDGE_EBT_T_NAT [=n]) 
 
 *   CONFIG_NET_ACT_POLICE:	 is not set when it should be.
	  │ Symbol: NET_ACT_POLICE [=n]                                                         │  
  │ Type  : tristate                                                                    │  
  │ Defined at net/sched/Kconfig:691                                                    │  
  │   Prompt: Traffic Policing                                                          │  
  │   Depends on: NET [=y] && NET_SCHED [=y] && NET_CLS_ACT [=y]                        │  
  │   Location:                                                                         │  
  │     -> Networking support (NET [=y])                                                │  
  │       -> Networking options                                                         │  
  │         -> QoS and/or fair queueing (NET_SCHED [=y])                                │  
  │           -> Actions (NET_CLS_ACT [=y])                                             │  
  │ (1)         -> Traffic Policing (NET_ACT_POLICE [=n])    
 
 
 
 *   CONFIG_NET_CLS_FW:	 is not set when it should be.
	  │ Symbol: NET_CLS_FW [=n]                                                             │  
  │ Type  : tristate                                                                    │  
  │ Defined at net/sched/Kconfig:482                                                    │  
  │   Prompt: Netfilter mark (FW)                                                       │  
  │   Depends on: NET [=y] && NET_SCHED [=y]                                            │  
  │   Location:                                                                         │  
  │     -> Networking support (NET [=y])                                                │  
  │       -> Networking options                                                         │  
  │         -> QoS and/or fair queueing (NET_SCHED [=y])                                │  
  │ (1)       -> Netfilter mark (FW) (NET_CLS_FW [=n])       
 
 
**** *   CONFIG_NET_CLS_U32:	 is not set when it should be.  ┌────────────────────────────────── Search Results ───────────────────────────────────┐
  │ Symbol: NET_CLS_U32 [=n]                                                            │  
  │ Type  : tristate                                                                    │  
  │ Defined at net/sched/Kconfig:492                                                    │  
  │   Prompt: Universal 32bit comparisons w/ hashing (U32)                              │  
  │   Depends on: NET [=y] && NET_SCHED [=y]                                            │  
  │   Location:                                                                         │  
  │     -> Networking support (NET [=y])                                                │  
  │       -> Networking options                                                         │  
  │         -> QoS and/or fair queueing (NET_SCHED [=y])                                │  
  │ (1)       -> Universal 32bit comparisons w/ hashing (U32) (NET_CLS_U32 [=n]) 
 
 
 
**** *   CONFIG_NET_SCH_HTB:	 is not set when it should be.
 │ Symbol: NET_SCH_HTB [=n]                                                            │  
  │ Type  : tristate                                                                    │  
  │ Defined at net/sched/Kconfig:48                                                     │  
  │   Prompt: Hierarchical Token Bucket (HTB)                                           │  
  │   Depends on: NET [=y] && NET_SCHED [=y]                                            │  
  │   Location:                                                                         │  
  │     -> Networking support (NET [=y])                                                │  
  │       -> Networking options                                                         │  
  │         -> QoS and/or fair queueing (NET_SCHED [=y])                                │  
  │ (1)       -> Hierarchical Token Bucket (HTB) (NET_SCH_HTB [=n])
  



 *   CONFIG_NET_SCH_INGRESS:	 is not set when it should be.
  .config - Linux/x86 6.6.21-gentoo Kernel Configuration
 > Search (CONFIG_NET_SCH_INGRESS) ────────────────────────────────────────────────────────
  ┌────────────────────────────────── Search Results ───────────────────────────────────┐
  │ Symbol: NET_SCH_INGRESS [=n]                                                        │  
  │ Type  : tristate                                                                    │  
  │ Defined at net/sched/Kconfig:347                                                    │  
  │   Prompt: Ingress/classifier-action Qdisc                                           │  
  │   Depends on: NET [=y] && NET_SCHED [=y] && NET_CLS_ACT [=y]                        │  
  │   Location:                                                                         │  
  │     -> Networking support (NET [=y])                                                │  
  │       -> Networking options                                                         │  
  │         -> QoS and/or fair queueing (NET_SCHED [=y])                                │  
  │ (1)       -> Ingress/classifier-action Qdisc (NET_SCH_INGRESS [=n])      
             │  
  │ Selects: NET_XGRESS [=y]                                                            │  
  │                                                                                     │  
  │                                                                                     │  
  │                               
 *   CONFIG_NET_SCH_SFQ:	 is not set when it should be.
 │ Symbol: NET_SCH_SFQ [=n]                                                            │  
  │ Type  : tristate                                                                    │  
  │ Defined at net/sched/Kconfig:111                                                    │  
  │   Prompt: Stochastic Fairness Queueing (SFQ)                                        │  
  │   Depends on: NET [=y] && NET_SCHED [=y]                                            │  
  │   Location:                                                                         │  
  │     -> Networking support (NET [=y])                                                │  
  │       -> Networking options                                                         │  
  │         -> QoS and/or fair queueing (NET_SCHED [=y])                                │  
  │ (1)       -> Stochastic Fairness Queueing (SFQ) (NET_SCH_SFQ [=n]) 







 * Install additional packages for optional runtime features:
 *   lxqt-base/lxqt-openssh-askpass for SSH_ASKPASS program implementation
 *   net-misc/ssh-askpass-fullscreen for SSH_ASKPASS program implementation
 *   net-misc/x11-ssh-askpass for SSH_ASKPASS program implementation
 *   app-emulation/qemu[usbredir,spice] for QEMU host support

 * Install additional packages for optional runtime features:
 *   lxqt-base/lxqt-openssh-askpass for SSH_ASKPASS program implementation
 *   net-misc/ssh-askpass-fullscreen for SSH_ASKPASS program implementation
 *   net-misc/x11-ssh-askpass for SSH_ASKPASS program implementation
 *   app-emulation/qemu[usbredir,spice] for QEMU host support


chmod u+s /usr/libexec/qemu-bridge-helper


spice

 * 
 * If you choose to enable the video streaming support of gstreamer,
 * please try to install addtional gst-plugins which matching the video codecs
 * 
 * mjpeg		media-plugins/gst-plugins-libav:1.0
 * vpx		media-plugins/gst-plugins-vpx:1.0
 * x264		media-plugins/gst-plugins-x264:1.0
 * 
 * (Note: Above message is only printed the first time package is
 * installed. Please look at /usr/share/doc/spice-0.15.2/README.gentoo*
 * for future reference)

 * GNU info directory index is up-to-date.
yoda /usr/src/linux # ^C


