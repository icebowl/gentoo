   /usr/bin/qemu-pr-helper

>>> Installing (8 of 8) app-emulation/qemu-8.1.5::gentoo
 * Running udev control --reload for reloading rules and databases ...                                                                                                                 [ ok ]
 * Updating icons cache ...                                                                                                                                                            [ ok ]
 * If you don't have kvm compiled into the kernel, make sure you have the
 * kernel module loaded before running kvm. The easiest way to ensure that the
 * kernel module is loaded is to load it on boot.
 * For AMD CPUs the module is called 'kvm-amd'.
 * For Intel CPUs the module is called 'kvm-intel'.
 * Please review /etc/conf.d/modules for how to load these.
 * 
 * Make sure your user is in the 'kvm' group. Just run
 * $ gpasswd -a <USER> kvm
 * then have <USER> re-login.
 * 
 * For brand new installs, the default permissions on /dev/kvm might not let
 * you access it.  You can tell udev to reset ownership/perms:
 * $ udevadm trigger -c add /dev/kvm
 * 
 * If you want to register binfmt handlers for qemu user targets:
 * For openrc:
 * # rc-update add qemu-binfmt
 * For systemd:
 * # ln -s /usr/share/qemu/binfmt.d/qemu.conf /etc/binfmt.d/qemu.conf
 * 
 * (Note: Above message is only printed the first time package is
 * installed. Please look at /usr/share/doc/qemu-8.1.5/README.gentoo*
 * for future reference)

>>> Recording app-emulation/qemu in "world" favorites file...

>>> Completed (8 of 8) app-emulation/qemu-8.1.5::gentoo

 * Messages for package sys-firmware/edk2-ovmf-bin-202202:

 * This package contains the tianocore edk2 UEFI firmware for 64-bit x86
 * virtual machines. The firmware is located under
 * /usr/share/edk2-ovmf/OVMF_CODE.fd
 * /usr/share/edk2-ovmf/OVMF_VARS.fd
 * /usr/share/edk2-ovmf/OVMF_CODE.secboot.fd
 * 
 * If USE=binary is enabled, we also install an OVMF variables file (coming from
 * fedora) that contains secureboot default keys
 * 
 * /usr/share/edk2-ovmf/OVMF_VARS.secboot.fd
 * 
 * If you have compiled this package by hand, you need to either populate all
 * necessary EFI variables by hand by booting
 * /usr/share/edk2-ovmf/UefiShell.(iso|img)
 * or creating OVMF_VARS.secboot.fd by hand:
 * https://github.com/puiterwijk/qemu-ovmf-secureboot
 * 
 * The firmware does not support csm (due to no free csm implementation
 * available). If you need a firmware with csm support you have to download
 * one for yourself. Firmware blobs are commonly labeled
 * OVMF{,_CODE,_VARS}-with-csm.fd
 * 
 * In order to use the firmware you can run qemu the following way
 * 
 * $ qemu-system-x86_64 		-drive file=/usr/share/edk2-ovmf/OVMF.fd,if=pflash,format=raw,unit=0,readonly=on 		...
 * 
 * (Note: Above message is only printed the first time package is
 * installed. Please look at /usr/share/doc/edk2-ovmf-bin-202202/README.gentoo*
 * for future reference)

 * Messages for package sys-firmware/ipxe-1.21.1:

 * Your configuration for sys-firmware/ipxe-1.21.1 has been saved in 
 * "/etc/portage/savedconfig/sys-firmware/ipxe-1.21.1" for your editing pleasure.
 * You can edit these files by hand and remerge this package with
 * USE=savedconfig to customise the configuration.
 * You can rename this file/directory to one of the following for
 * its configuration to apply to multiple versions:
 * ${PORTAGE_CONFIGROOT}/etc/portage/savedconfig/
 * [${CTARGET}|${CHOST}|""]/${CATEGORY}/[${PF}|${P}|${PN}]

 * Messages for package app-emulation/qemu-8.1.5:

 * If you don't have kvm compiled into the kernel, make sure you have the
 * kernel module loaded before running kvm. The easiest way to ensure that the
 * kernel module is loaded is to load it on boot.
 * For AMD CPUs the module is called 'kvm-amd'.
 * For Intel CPUs the module is called 'kvm-intel'.
 * Please review /etc/conf.d/modules for how to load these.
 * 
 * Make sure your user is in the 'kvm' group. Just run
 * $ gpasswd -a <USER> kvm
 * then have <USER> re-login.
 * 
 * For brand new installs, the default permissions on /dev/kvm might not let
 * you access it.  You can tell udev to reset ownership/perms:
 * $ udevadm trigger -c add /dev/kvm
 * 
 * If you want to register binfmt handlers for qemu user targets:
 * For openrc:
 * # rc-update add qemu-binfmt
 * For systemd:
 * # ln -s /usr/share/qemu/binfmt.d/qemu.conf /etc/binfmt.d/qemu.conf
 * 
 * (Note: Above message is only printed the first time package is
 * installed. Please look at /usr/share/doc/qemu-8.1.5/README.gentoo*
 * for future reference)

 * GNU info directory index is up-to-date.
yoda ~ # 

emerge vit-manager
The following USE changes are necessary to proceed:
 (see "package.use" in the portage(5) man page for more details)
# required by app-emulation/virt-manager-4.1.0-r1::gentoo[-test,gui]
# required by app-emulation/virt-manager (argument)
>=net-misc/spice-gtk-0.42-r3 usbredir
# required by app-emulation/libvirt-9.8.0-r2::gentoo[virt-network]
# required by app-emulation/libvirt-glib-4.0.0::gentoo
# required by app-emulation/virt-manager-4.1.0-r1::gentoo[-test]
# required by app-emulation/virt-manager (argument)
>=net-dns/dnsmasq-2.90 script
# required by app-crypt/swtpm-0.8.1-r2::gentoo
# required by app-emulation/libvirt-9.8.0-r2::gentoo[qemu]
# required by app-emulation/libvirt-glib-4.0.0::gentoo
# required by app-emulation/virt-manager-4.1.0-r1::gentoo[-test]
# required by app-emulation/virt-manager (argument)
>=net-libs/gnutls-3.8.0 pkcs11 tools

Calculating dependencies... done!
Dependency resolution took 17.84 s (backtrack: 0/20).

[ebuild  N     ] acct-group/radvd-0-r2 
[ebuild  N     ] acct-group/dnsmasq-0-r3 
[ebuild  N     ] acct-group/tss-0-r3 
[ebuild  N     ] app-misc/scrub-2.6.1-r1 
[ebuild  N     ] acct-group/libvirt-0-r3 
[ebuild  N     ] net-misc/ethertypes-0 
[ebuild  N     ] acct-group/qemu-0-r3 
[ebuild  N     ] acct-user/qemu-0-r3 
[ebuild  N     ] acct-user/radvd-0-r2 
[ebuild  N     ] acct-user/dnsmasq-0-r3 
[ebuild  N     ] acct-user/tss-0-r3 
[ebuild  N     ] net-analyzer/openbsd-netcat-1.219_p1 
[ebuild  N     ] app-text/xhtml1-20020801-r6 
[ebuild   R    ] net-libs/gnutls-3.8.0  USE="pkcs11* tools*" 
[ebuild  N     ] net-dns/dnsmasq-2.90  USE="dbus dhcp dumpfile inotify ipv6 loop nls script -auth-dns -conntrack -dhcp-tools -dnssec -id -idn -libidn2 -lua -nettlehash (-selinux) -static -tftp" LUA_SINGLE_TARGET="lua5-1 -lua5-3 -lua5-4 -luajit" 
[ebuild  N     ] net-libs/rpcsvc-proto-1.4.4 
[ebuild  N     ] dev-python/pyparsing-3.1.2  USE="-examples -test" PYTHON_TARGETS="python3_11 (-pypy3) -python3_10 -python3_12" 
[ebuild  N     ] dev-libs/yajl-2.1.0-r4  ABI_X86="(64) -32 (-x32)" 
[ebuild  N     ] sys-apps/usbredir-0.13.0-r1  USE="-test" 
[ebuild  N     ] app-emulation/spice-protocol-0.14.4 
[ebuild  N     ] media-libs/graphene-1.10.8  USE="introspection -doc -test" ABI_X86="(64) -32 (-x32)" CPU_FLAGS_X86="sse2" 
[ebuild  N     ] sys-apps/osinfo-db-tools-1.11.0  USE="-test" 
[ebuild  N     ] dev-perl/Text-CSV_XS-1.520.0  USE="-examples -test" 
[ebuild  N     ] sys-apps/osinfo-db-20231215 
[ebuild  N     ] sys-libs/libosinfo-1.11.0  USE="introspection vala -gtk-doc -test" 
[ebuild  N     ] dev-perl/Text-CSV-2.30.0  USE="xs -test" 
[ebuild  N     ] net-firewall/ebtables-2.0.11-r3  USE="perl -static" 
[ebuild  N     ] dev-libs/libtpms-0.9.6 
[ebuild  N     ] dev-libs/libisoburn-1.5.6-r1  USE="acl readline xattr zlib -debug -external-filters -external-filters-setuid -frontend-optional -launch-frontend -launch-frontend-setuid -libedit -static-libs" 
[ebuild  N     ] app-crypt/swtpm-0.8.1-r2  USE="seccomp -fuse -test" 
[ebuild  N     ] net-libs/gtk-vnc-1.3.1  USE="introspection vala -pulseaudio -sasl" 
[ebuild  N     ] dev-python/argcomplete-3.2.3  USE="-test" PYTHON_TARGETS="python3_11 (-pypy3) -python3_10 -python3_12" 
[ebuild  N     ] dev-libs/libnl-3.8.0  USE="debug -python -test -utils" ABI_X86="(64) -32 (-x32)" PYTHON_TARGETS="python3_11 -python3_10" 
[ebuild  N     ] net-misc/radvd-2.19-r5  USE="(-selinux) -test" 
[ebuild  N     ] media-libs/gstreamer-1.22.11  USE="caps introspection nls -test -unwind" ABI_X86="(64) -32 (-x32)" 
[ebuild  N     ] media-libs/gst-plugins-base-1.22.11  USE="X alsa egl gles2 introspection nls ogg opengl orc pango vorbis -gbm -ivorbis -test -theora -wayland" ABI_X86="(64) -32 (-x32)" 
[ebuild  N     ] media-libs/gst-plugins-good-1.22.11  USE="nls orc -test" ABI_X86="(64) -32 (-x32)" 
[ebuild  N     ] net-misc/spice-gtk-0.42-r3  USE="gtk3 introspection policykit usbredir -gtk-doc -lz4 -mjpeg -sasl -smartcard -vala -valgrind -wayland -webdav" 
[ebuild  N     ] dev-perl/XML-XPath-1.480.0  USE="-examples -test" 
[ebuild  N     ] app-emulation/libvirt-9.8.0-r2  USE="caps libvirtd nls policykit qemu udev virt-network -apparmor -audit -bash-completion -dtrace -firewalld -fuse -glusterfs -iscsi -iscsi-direct -libssh -libssh2 -lvm -lxc -nfs -numa (-openvz) -parted -pcap -rbd -sasl (-selinux) -test -verify-sig -virtualbox -wireshark-plugins -xen -zfs" 
[ebuild  N     ] dev-python/libvirt-python-9.8.0  USE="-debug -examples -test -verify-sig" PYTHON_TARGETS="python3_11 -python3_10 -python3_12" 
[ebuild  N     ] app-emulation/libvirt-glib-4.0.0  USE="introspection vala -gtk-doc -test" 
[ebuild  N     ] app-emulation/virt-manager-4.1.0-r1  USE="gui policykit -sasl -test" PYTHON_SINGLE_TARGET="python3_11 -python3_10" 

post emerge libvert

 * Messages for package app-emulation/libvirt-9.8.0-r2:

 *   CONFIG_BRIDGE_EBT_MARK_T:	 is not set when it should be.
 *   CONFIG_BRIDGE_NF_EBTABLES:	 is not set when it should be.
 *   CONFIG_NETFILTER_ADVANCED:	 is not set when it should be.
 *   CONFIG_NETFILTER_XT_CONNMARK:	 is not set when it should be.
 *   CONFIG_NETFILTER_XT_TARGET_CHECKSUM:	 is not set when it should be.
 *   CONFIG_IP6_NF_NAT:	 is not set when it should be.
 *   CONFIG_BRIDGE_EBT_T_NAT:	 is not set when it should be.
 *   CONFIG_NET_ACT_POLICE:	 is not set when it should be.
 *   CONFIG_NET_CLS_FW:	 is not set when it should be.
 *   CONFIG_NET_CLS_U32:	 is not set when it should be.
 *   CONFIG_NET_SCH_HTB:	 is not set when it should be.
 *   CONFIG_NET_SCH_INGRESS:	 is not set when it should be.
 *   CONFIG_NET_SCH_SFQ:	 is not set when it should be.
 * Please check to make sure these options are set correctly.
 * Failure to do so may cause unexpected problems.
 * Important: The openrc libvirtd init script is now broken up into two
 * separate services: libvirtd, that solely handles the daemon, and
 * libvirt-guests, that takes care of clients during shutdown/restart of the
 * host. In order to reenable client handling, edit /etc/conf.d/libvirt-guests
 * and enable the service and start it:
 * 
 * $ rc-update add libvirt-guests
 * $ rc-service libvirt-guests start
 * 
 * 
 * For the basic networking support (bridged and routed networks) you don't
 * need any extra software. For more complex network modes including but not
 * limited to NATed network, you can enable the 'virt-network' USE flag. It
 * will pull in required runtime dependencies
 * 
 * 
 * If you are using dnsmasq on your system, you will have to configure
 * /etc/dnsmasq.conf to enable the following settings:
 * 
 * bind-interfaces
 * interface or except-interface
 * 
 * Otherwise you might have issues with your existing DNS server.
 * 
 * 
 * For openrc users:
 * 
 * Please use /etc/conf.d/libvirtd to control the '--listen' parameter for
 * libvirtd.
 * 
 * Use /etc/init.d/libvirt-guests to manage clients on restart/shutdown of
 * the host. The default configuration will suspend and resume running kvm
 * guests with 'managedsave'. This behavior can be changed under
 * /etc/conf.d/libvirt-guests
 * 
 * 
 * For systemd users:
 * 
 * The '--listen' parameter is unavailable when libvirtd is run as a
 * systemd unit.
 * 
 * The configuration for the 'libvirt-guests.service' is found under
 * /etc/libvirt/libvirt-guests.conf"
 * 
 * 
 * If you have built libvirt with policykit support, a new group "libvirt" has
 * been created. Simply add a user to the libvirt group in order to grant
 * administrative access to libvirtd. Alternatively, drop a custom policykit
 * rule into /etc/polkit-1/rules.d.
 * 
 * If you have built libvirt without policykit support (USE=-policykit), you
 * must change the unix sock group and/or perms in /etc/libvirt/libvirtd.conf
 * in order to allow normal users to connect to libvirtd.
 * 
 * 
 * If libvirt is built with USE=caps, libvirt will now start qemu/kvm VMs
 * with non-root privileges. Ensure any resources your VMs use are accessible
 * by qemu:qemu.
 * 
 * (Note: Above message is only printed the first time package is
 * installed. Please look at /usr/share/doc/libvirt-9.8.0-r2/README.gentoo*
 * for future reference)

 * Messages for package app-emulation/virt-manager-4.1.0-r1:

 * Install additional packages for optional runtime features:
 *   lxqt-base/lxqt-openssh-askpass for SSH_ASKPASS program implementation
 *   net-misc/ssh-askpass-fullscreen for SSH_ASKPASS program implementation
 *   net-misc/x11-ssh-askpass for SSH_ASKPASS program implementation
 *   app-emulation/qemu[usbredir,spice] for QEMU host support

 * Regenerating GNU info directory index...
 * Processed 125 info files.
yoda ~ # 

