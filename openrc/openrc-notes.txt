https://wiki.gentoo.org/wiki/OpenRC_to_systemd_Cheatsheet
Start a service	/etc/init.d/<service> start
rc-service <service> start 
Stop a service	/etc/init.d/<service> stop
rc-service <service> stop		
Restart a service	/etc/init.d/<service> restart
rc-service <service> restart		
Get service status	/etc/init.d/<service> status
rc-service <service> status	
Show known startup scripts	rc-status
rc-update show 
Show all startup scripts	ls /etc/init.d/
rc-update -v show 
Enable service at startup	rc-update add <service> <runlevel> 
Disable service at startup	rc-update del <service> <runlevel>	
