https://bugs.gentoo.org/707026

eselect locale list
Available targets for the LANG variable:
  [1]   C
  [2]   C.utf8
  [3]   en_US
  [4]   en_US.iso88591
  [5]   en_US.utf8 *
  [6]   POSIX
  [ ]   (free form)

locale-gen

env-update && source /etc/profile


emerge -av -1 gnome-doc-utils

