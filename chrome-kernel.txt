
>>> Completed (14 of 14) x11-libs/libXpm-3.5.16::gentoo

 * Messages for package x11-drivers/xf86-video-ati-22.0.0:

 * Could not find a Makefile in the kernel source directory.
 * Please ensure that /usr/src/linux points to a complete set of Linux sources
 * Unable to calculate Linux Kernel version for build, attempting to use running version
 * Unable to check for the following kernel config options due
 * to absence of any configured kernel sources or compiled
 * config:
 *  - DRM_RADEON_UMS
 *  - FB_RADEON
 * You're on your own to make sure they are set if needed.

 * Messages for package x11-drivers/xf86-video-ati-22.0.0:

 * Could not find a Makefile in the kernel source directory.
 * Please ensure that /usr/src/linux points to a complete set of Linux sources
 * Unable to calculate Linux Kernel version for build, attempting to use running version

 * GNU info directory index is up-to-date.
 * After world updates, it is important to remove obsolete packages with
 * emerge --depclean. Refer to `man emerge` for more information.


230606
>>> Completed (2 of 2) www-client/google-chrome-114.0.5735.106::gentoo

 * Messages for package www-client/firefox-bin-114.0:

 * Could not find a Makefile in the kernel source directory.
 * Please ensure that /usr/src/linux points to a complete set of Linux sources
 * Unable to calculate Linux Kernel version for build, attempting to use running version
 * Unable to check for the following kernel config options due
 * to absence of any configured kernel sources or compiled
 * config:
 *  - SECCOMP - CONFIG_SECCOMP not set! This system will be unable to play DRM-protected content.
 * You're on your own to make sure they are set if needed.

 * Messages for package www-client/google-chrome-114.0.5735.106:

 * Could not find a Makefile in the kernel source directory.
 * Please ensure that /usr/src/linux points to a complete set of Linux sources
 * Unable to calculate Linux Kernel version for build, attempting to use running version
 * Unable to check for the following kernel config options due
 * to absence of any configured kernel sources or compiled
 * config:
 *  - PID_NS - PID_NS is required for sandbox to work
 *  - NET_NS - NET_NS is required for sandbox to work
 *  - SECCOMP_FILTER - SECCOMP_FILTER is required for sandbox to work
 *  - USER_NS - USER_NS is required for sandbox to work
 *  - ADVISE_SYSCALLS - CONFIG_ADVISE_SYSCALLS is required for the renderer (bug #552576)
 *  - COMPAT_VDSO - CONFIG_COMPAT_VDSO causes segfaults (bug #556286)
 *  - GRKERNSEC - CONFIG_GRKERNSEC breaks sandbox (bug #613668)
 * You're on your own to make sure they are set if needed.

 * GNU info directory index is up-to-date.
 * After world updates, it is important to remove obsolete packages with
 * emerge --depclean. Refer to `man emerge` for more information.
