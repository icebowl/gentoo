>>> sys-apps/man-db-2.11.1 merged.

 * Messages for package net-wireless/bluez-5.66:

 *   CONFIG_BT:	 is not set when it should be.
 *   CONFIG_BT_RFCOMM:	 is not set when it should be.
 *   CONFIG_BT_RFCOMM_TTY:	 is not set when it should be.
 *   CONFIG_BT_BNEP:	 is not set when it should be.
 *   CONFIG_BT_BNEP_MC_FILTER:	 is not set when it should be.
 *   CONFIG_BT_BNEP_PROTO_FILTER:	 is not set when it should be.
 *   CONFIG_BT_HIDP:	 is not set when it should be.
 *   CONFIG_CRYPTO_USER_API_HASH:	 is not set when it should be.
 *   CONFIG_CRYPTO_USER_API_SKCIPHER:	 is not set when it should be.
 *   CONFIG_CRYPTO_USER:	 is not set when it should be.
 *   CONFIG_CRYPTO_USER_API:	 is not set when it should be.
 *   CONFIG_CRYPTO_USER_API_AEAD:	 is not set when it should be.
 *   CONFIG_KEY_DH_OPERATIONS:	 is not set when it should be.
 * Please check to make sure these options are set correctly.
 * Failure to do so may cause unexpected problems.
 * To use dial up networking you must install net-dialup/ppp

 * GNU info directory index is up-to-date.

 * IMPORTANT: 2 news items need reading for repository 'gentoo'.
 * Use eselect news read to view new items.

 * After world updates, it is important to remove obsolete packages with
 * emerge --depclean. Refer to `man emerge` for more information.
qed ~ # 
