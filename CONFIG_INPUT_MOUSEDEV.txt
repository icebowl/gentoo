 * Messages for package sys-libs/gpm-1.20.7-r6:

 *   CONFIG_INPUT_MOUSEDEV:	is not set (required to expose mice for GPM)
 * Please check to make sure these options are set correctly.
 * Failure to do so may cause unexpected problems.

 * Messages for package sys-libs/gpm-1.20.7-r6:

 *   CONFIG_INPUT_MOUSEDEV:	is not set (required to expose mice for GPM)
 * Please check to make sure these options are set correctly.
 * Failure to do so may cause unexpected problems.

 * Messages for package sys-libs/pam-1.5.3-r1:

 * Some software with pre-loaded PAM libraries might experience
 * warnings or failures related to missing symbols and/or versions
 * after any update. While unfortunate this is a limit of the
 * implementation of PAM and the software, and it requires you to
 * restart the software manually after the update.
 * 
 * You can get a list of such software running a command like
 *   lsof / | grep -E -i 'del.*libpam\.so'
 * 
 * Alternatively, simply reboot your system.


 Symbol: INPUT_MOUSEDEV [=n]                                                                                                                                                           │  
  │ Type  : tristate                                                                                                                                                                      │  
  │ Defined at drivers/input/Kconfig:89                                                                                                                                                   │  
  │   Prompt: Mouse interface                                                                                                                                                             │  
  │   Depends on: INPUT [=y]                                                                                                                                                              │  
  │   Location:                                                                                                                                                                           │  
  │     -> Device Drivers                                                                                                                                                                 │  
  │       -> Input device support                                                                                                                                                         │  
  │         -> Generic input layer (needed for keyboard, mouse, ...) (INPUT [=y])                                                                                                         │  
  │ (1)       -> Mouse interface (INPUT_MOUSEDEV [=n])                                                                                                                                    │  
  │                                                          
